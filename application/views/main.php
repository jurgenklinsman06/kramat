<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url() ?>vendor/bootstrap/css/bootstrap.min.yeti.css">
  
  <script src="<?php echo base_url() ?>vendor/jquery/jquery.min.js"></script>
  
  <script src="<?php echo base_url() ?>vendor/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<?php $this->load->view('menu'); ?>
<br>
<div class="container">
  
  <div class="card border-primary" >
    <div class="card-header"><?php echo $this->uri->segment(1); ?></div>
    <div class="card-body">
      <?php $this->load->view($page); ?>
    </div>
  </div>
</div>

</body>
</html>